#ifndef HEADER_H
#define HEADER_H

#define BOARD_SIZE_X 16
#define BOARD_SIZE_Y 16
#define MAX_LENGTH BOARD_SIZE_X * BOARD_SIZE_Y

#define UP 0
#define LEFT 1
#define DOWN 2
#define RIGHT 3

#endif //delimits #ifndef HEADER_H
