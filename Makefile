##########################
# danger noodle makefile #
##########################

NAME = snake

# Tools
TOOLS_DIR = tools
BFIN = $(TOOLS_DIR)/bfin-elf/bin
BFLOD = $(TOOLS_DIR)/Bflod/bflod

# includes
INC_DIRS = include # blackfin headers includes
#INC_DIRS += . # project header includes

INCLUDES = $(addprefix -I, $(INC_DIRS))	# add -I prefix to all include directories
LDF = include/bfkit.ldf		#Link File (Memory structure)

CC = $(BFIN)/bfin-elf-gcc	#GCC Compiler
LD = $(BFIN)/bfin-elf-ld	#Linker

# compiled source files
OFILES = snake.o	#bundles source files
OFILES += snakeMechanics.o
OFILES += draw.o
OFILES += uart.o
OFILES += prng.o

#----------------------------------------------------------

all: $(NAME).hex

%.o: %.S
	$(CC) -c $(INCLUDES) $< #compiles all source files

$(NAME).x: $(OFILES)
	$(LD) -T $(LDF) -o $@ $^ #creates executable from all compiled source files using the linker

$(NAME).hex: $(NAME).x
	$(BFIN)/bfin-elf-objcopy -O ihex $< $@ #rewrites into uploadable format "intel hex loader file"

run: $(NAME).hex
	$(BFLOD) -t $(NAME).hex #transmits final program and starts bflod terminal

run_putty: $(NAME).hex
	$(BFLOD) $(NAME).hex #transmits final program
	putty -load Snake

clean:
	rm -f *.o *.x *.hex
