#include <stdio.h>
#include <unistd.h>
#define MAX_LENGTH 10
#define BOARD_SIZE 30

#define UP 0
#define LEFT 1
#define DOWN 2
#define RIGHT 3


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//CIRCULAR BUFFER EMULATOR (fill with invalid number) snakeCircularBuffer[index] is x component, index+1 is y component
typedef struct {
	int data [MAX_LENGTH*2];
	int index; //SHOULD ALWAYS POINT TO HEAD AFTER ANY ITERATION
}circularBuffer;
circularBuffer snake;
int length;

void initialize(){
	snake.index = 0;
	for (int i = 0; i < MAX_LENGTH*2; i++){
		snake.data[i] = -1; //INVALID, MAY ALSO BE POSITIVELY OUTSIDE OF BOARD
	}
}
//INCREMENT IS POSITION (JUMPS OVER X AND Y)
void shiftIndex(int increment){
	snake.index += increment*2;

	if (snake.index >= MAX_LENGTH*2)
		snake.index -= MAX_LENGTH*2;

	if (snake.index < 0)
		snake.index += MAX_LENGTH*2; 		
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

char direction;

char onSnake(int x, int y){//(1,0) = (true, false)
	for (int i = 0; i < length; i++){
		if (snake.data[snake.index] == x && snake.data[snake.index+1] == y){
			shiftIndex(-i);
			return 1;
		}
		shiftIndex(1);
	}	
	shiftIndex(-length);
	return 0;
}

void display(){
	printf("\x1b[2J");	
	for (int j = BOARD_SIZE; j >= -1; j--){
		for (int i = -1; i <= BOARD_SIZE; i++){
			if (j == -1 || j == BOARD_SIZE){//BORDER TOP BOTTOM
				printf("-");
				continue;
			}
			if (i == -1 || i == BOARD_SIZE){//BORDER LEFT RIGHT
				printf("|");
				continue;
			}
							
			if (onSnake(i,j))
				printf("o");
			else
				printf(" ");
		}
		printf("\n");
	}
}

void moveSnake(){
	int x = snake.data[snake.index];
	int y = snake.data[snake.index+1];
	if (direction == UP)
		y++;
	if (direction == DOWN)
		y--;
	if (direction == RIGHT)
		x++;
	if (direction == LEFT)
		x--;
	
	if (x >= BOARD_SIZE)
		x -= BOARD_SIZE;
	if (x < 0)
		x += BOARD_SIZE;
	if (y >= BOARD_SIZE)
		y -= BOARD_SIZE;
	if (y < 0)
		y += BOARD_SIZE;

	shiftIndex(-1);//New head
	snake.data[snake.index] = x;
	snake.data[snake.index+1] = y; 

	shiftIndex(length);
	snake.data[snake.index] = -1;//REMOVE TAIL
	snake.data[snake.index+1] = -1;
	shiftIndex(-length);
}

void update(){ //WILL HAVE MORE FUNCTIONS IN FUTURE (APPLE ETC)
	moveSnake();	
	display();
}

void leftButton(){
	direction++;
	if (direction > RIGHT)
		direction = UP;
}

void rightButton(){
	direction--;
	if (direction < UP)
		direction = RIGHT;
}

int main(){
	initialize();
	snake.data[0] = 3;
	snake.data[1] = 2;
	snake.data[2] = 3;
	snake.data[3] = 3;
	snake.data[4] = 3;
	snake.data[5] = 4;
	snake.data[6] = 3;
	snake.data[7] = 5;
	direction = DOWN;
	length = 4;
	while(1){
		char input;	
		scanf("%c", &input);
		if (input == 'a')
			leftButton();
		if (input == 'd')
			rightButton();
		update();
	}
	return 0;
}
