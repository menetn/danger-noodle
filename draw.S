#include "header.h"
#include <defBF532.h>

#define FOOD_CHAR '$'
#define SNAKE_BODY_CHAR 'o'
#define V_LINE 'X'
#define H_LINE 'X'

//#define PUTTY

.data
.section    .rodata 			//read-only constant data
#ifdef PUTTY
eraseScreen: .string "\x1b[32A\r";
#else
eraseScreen: .string "\n\n\n\n";
#endif
newLine: .string "\n\r";

//I0 Head Pointer, R0 send/receive character, P0 send string

.text

.global initDraw
initDraw:
	[--SP] = RETS;
	[--SP] = (R7:0, P5:0);
	
	CALL initx;			//Initialize uart (R0/P0 reserved for output)
	R0 = 0 (Z);
	CALL txchr;

	(R7:0, P5:0) = [SP++];
	RETS = [SP++]
	RTS;

.global draw
draw:
	[--SP] = RETS;
	[--SP] = (R7:0, P5:0);
	
	P0.H = eraseScreen;
	P0.L = eraseScreen;
	CALL txstr;

	R2.H = HI(BOARD_SIZE_X);
	R2.L = LO(BOARD_SIZE_X);
	R1.H = HI(BOARD_SIZE_Y);  
	R1.L = LO(BOARD_SIZE_Y);

	//Draw upper border and new line
	R3 = 2 (Z);
	R3 = R3 + R2; 
	LC0 = R3;
	LOOP upperBorder LC0;
	LOOP_BEGIN upperBorder; 		//Loop LC0 (x-Coord) from BOARD_SIZE_X to 1
		R0 = H_LINE;
		CALL txchr;
	LOOP_END upperBorder;
	P0.H = newLine;
	P0.L = newLine;
	CALL txstr;

	//Draw GameBoard
	LC0 = R1;
	LOOP yCoords LC0;
	LOOP_BEGIN yCoords; 		//Loop LC0 (y-Coord) from BOARD_SIZE_Y to 1
		//Left border
		R0 = V_LINE;
		CALL txchr;

		LC1 = R2;		 
		LOOP xCoords LC1;        
		LOOP_BEGIN xCoords; 		//Loop LC1 (x-Coord) from BOARD_SIZE_X to 1
			R0 = LC1;		//x Coord/HIGH position vector for onsnake method
			R3 = LC0;		//y Coord/LOW
			R0 = R0 << 16;
			R0 = R0 | R3;
			CALL onFood;
			IF !CC JUMP checkOnSnake;
			R0 = FOOD_CHAR;
			JUMP drawElement;

checkOnSnake:
			CALL onSnake;

			IF CC JUMP setSnake;
			R0 = ' ';
			JUMP drawElement;
			setSnake:
			R0 = SNAKE_BODY_CHAR;
			drawElement:
			CALL txchr;
			
		LOOP_END xCoords;
		
		//Right border and new line
		R0 = V_LINE;
		CALL txchr;
		P0.H = newLine;
		P0.L = newLine;
		CALL txstr;
	LOOP_END yCoords;
		
	//Draw lower border 
	R3 = 2 (Z);
	R3 = R3 + R2;
	LC0 = R3;			 
	LOOP lowerBorder LC0;
	LOOP_BEGIN lowerBorder; 		//Loop LC0 (x-Coord) from BOARD_SIZE_X to 1
		R0 = H_LINE;
		CALL txchr;
	LOOP_END lowerBorder;

	(R7:0, P5:0) = [SP++];
	RETS = [SP++]
	RTS;
